function CloneObject(object) {
    return JSON.parse(JSON.stringify(object))
}

function Merge2Objects(original,newObj) {
    return {...original, ...newObj}
}

function getRecordById(arrayOfObjects, _id){
    let result = false
    for (const iterator of arrayOfObjects) {
        if(iterator.id === _id){
            result = iterator
            break
        }
    }
    return result
} 

function getRecordNumberById(arrayOfObjects, _id){
    let result = false
    let i = 0
    for (const iterator of arrayOfObjects) {
        if(iterator.id === _id){
            result = i
            break
        }
        i++
    }
    return result
} 

function getColumnsFromObjArray(arr) {
    const columns = []
    for (const iterator of arr) {
        for (const key of Object.keys(iterator)) {
          if (!columns.includes(key)) {
            columns.push(key);
          }
        }
    }
    return columns
  }

export {CloneObject,Merge2Objects,getRecordById,getRecordNumberById,getColumnsFromObjArray}