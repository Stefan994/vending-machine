const keyPadValuesAbove0 = [1,2,3,4,5,6,7,8,9]
const maxLimitOnScreen = 2
const initialProducts = [
    {
        id:1,
        label:'Cola',
        logoName:'cocacola',
        price:3,
        qty:15,
    },
    {
        id:2,
        label:'Sprite',
        logoName:'sprite',
        price:2,
        qty:18,
    },
    {
        id:3,
        label:'Fanta',
        logoName:'fanta',
        price:1,
        qty:15,
    },
    {
        id:4,
        label:'Pepsi',
        logoName:'Pepsi',
        price:3,
        qty:18,
    },
    {
        id:10,
        label:'Snickers',
        logoName:'snickers',
        price:4,
        qty:20,
    },
    {
        id:11,
        label:'Twix',
        logoName:'twix',
        price:5,
        qty:15,
    },
    {
        id:12,
        label:'Lion',
        logoName:'lion',
        price:2,
        qty:18,
    },
    {
        id:14,
        label:'Lion Vanilla',
        logoName:'lion',
        price:3,
        qty:20,
    },
    {
        id:20,
        label:'Lays (salty)',
        logoName:'Lays',
        price:1,
        qty:15,
    },
    {
        id:21,
        label:'Lays (oven)',
        logoName:'Lays',
        price:3,
        qty:17,
    },
    {
        id:22,
        label:'Cheetos (salty)',
        logoName:'Cheetos',
        price:3,
        qty:18,
    },
    {
        id:23,
        label:'Cheetos (chesee)',
        logoName:'Cheetos',
        price:2,
        qty:20,
    },
    {
        id:30,
        label:'7Days (chocolate)',
        logoName:'7Days',
        price:2,
        qty:18,
    },
    {
        id:31,
        label:'7Days (vanilla)',
        logoName:'7Days',
        price:3,
        qty:17,
    },
    {
        id:32,
        label:'7Days (cherry)',
        logoName:'7Days',
        price:5,
        qty:15,
    },
    {
        id:33,
        label:'7Days (champagne)',
        logoName:'7Days',
        price:3,
        qty:18,
    },
    {
        id:40,
        label:'M&Ms (chocolate)',
        logoName:'mms',
        price:4,
        qty:17,
    },
    {
        id:41,
        label:'M&Ms (peanuts)',
        logoName:'mms',
        price:3,
        qty:20,
    },
    {
        id:42,
        label:'Mentos',
        logoName:'mentos',
        price:1,
        qty:15,
    },
    {
        id:43,
        label:'Mentos (xtreme)',
        logoName:'mentos',
        price:2,
        qty:18,
    },

]
const coins = [
    {
        color : '#FFFF91',
        value : 1,
        dimension : 50,
    },
    {
        color : '#eeff05',
        value : 5,
        dimension : 55,
    },
    {
        color : '#ffde05',
        value : 10,
        dimension : 61,
    }
]

const instructions = [
    {
        Label: 'II',
        'Short desc.': 'Machine ready'
    },
    {
        Label: 'XX',
        'Short desc.': 'Not enough money'
    },
    {
        Label: 'QQ',
        'Short desc.': 'Selected item empty'
    },
]


export {keyPadValuesAbove0,maxLimitOnScreen,initialProducts,coins,instructions}