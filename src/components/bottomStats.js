import Table from "./table";

function BottomStats(props) {
  const {bought,title} = props
  const cleanedBought = bought.map(x=>{
    return{Name:x.label, Quantity:x.qty}
    })
  const moneySpent = bought.reduce(
    (accumulator, current)=>{
      return accumulator + current.price * current.qty
    },0
  )
  return (
    <div>
        <Table data = {cleanedBought} title = {title}/>
        <p>
          Money spent: <b>{moneySpent}</b>
        </p>
    </div>
    );
  }
  
  export default BottomStats;
  