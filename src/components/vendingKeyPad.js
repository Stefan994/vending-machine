import { keyPadValuesAbove0 } from "../config";
import  VendingButton  from "./vendingButton";
function VendingKeyPad(props) {
    
    const {buttonPress,canPurchase,buy,clear} = props
    return (
      <div className = 'keyPadContainer'>
          {keyPadValuesAbove0.map(
              x=><VendingButton  label = {x} key = {x} buttonPress = {buttonPress} _id = {x.toString()}/> 
          )}
          <VendingButton  label = 'C' key = {10} _id = '10' buttonPress = {clear}/> 
          <VendingButton  label = {0} key = {0} _id = '0' buttonPress = {buttonPress}/>
          <VendingButton  key = {11} _id = 'buy' buttonPress = {buy} canPurchase = {canPurchase} canBeDisabled={true}/>   
      </div>
    );
  }
  
  export default VendingKeyPad;
  