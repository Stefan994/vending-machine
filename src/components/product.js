function Product(props) {

    const {productObject} = props

    const addDefaultSrc = function(e){
      e.target.src = './noPic.png'
    }

    return (
      <div className = 'prod'>
        <img 
          className = 'prodImg'
           src = {`https://logo.clearbit.com/${productObject.logoName}.com`}
           alt = 'Product'
           onError={addDefaultSrc}
            />
        <div className = 'textInRight'>
          <div>
            {productObject.label}
          </div>
          <div >
            Code: <b>{productObject.id}</b>
              
            
          </div>
          <div>
            QTY: <b>{productObject.qty}</b>
          </div>
          <div>
            Price : <b>{productObject.price}</b>
          </div>
        </div>
      </div>
    );
  }
  
  export default Product;
  