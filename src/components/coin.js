
function Coin(props) {
    const {value,dimension,color,coinClickHandle} = props
    return (
      <div className = 'coin noSelect' onClick = {() => coinClickHandle(value)}>
          <svg width={dimension} height={dimension} xmlns="http://www.w3.org/2000/svg">
          <g>
            <ellipse fill={color} stroke="#000" cx={dimension/2} cy={dimension/2} id="svg_1" rx={dimension/2} ry={dimension/2} strokeWidth="2"/>
            <text x="50%" y="50%" textAnchor="middle" stroke="#333" strokeWidth="2px" dy=".35em" fontSize={dimension/1.5}>{value}</text>
          </g>

          </svg>
      </div>
    );
  }
  
  export default Coin;
  