import Product from "./product";

function Products(props) {
    const {products} = props
    // console.log(products);
    return (
      <div className = 'productContainer'>
          {products.map(
            x=><Product productObject = {x} key = {x.id}/>
          )}
      </div>
    );
  }
  
  export default Products;
  