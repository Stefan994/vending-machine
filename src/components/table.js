import {getColumnsFromObjArray} from "../utils/objectUtils"

function Table(props) {
  const {title,data} = props
  const columns = getColumnsFromObjArray(data)
  return (
    <div className = 'tableContainer'>
          <h3>
            {title}
          </h3>
          <table className = 'table center'>
            <thead>
              <tr>
                {columns.map(x=><th key = {columns.indexOf(x)}>{x}</th>)}  
              </tr>      
            </thead>
            <tbody>
              {
                data.map(x=>{
                  return (
                    <tr key= {100+data.indexOf(x)*100}>
                      {columns.map(y=><td  key = {columns.indexOf(y)+data.indexOf(x)*10}>{x[y]}</td>)} 
                    </tr>
                  )
                })
              }
            </tbody>
          </table>
      </div>
    );
  }
  
  export default Table;