import  Coin  from "./coin";
import { coins } from "../config";


function Inventory(props) {
  const {coinClickHandle,funds} = props
  return (
    <div className = 'fundContainer'>
          <h2>
            Funds: <b>{funds}</b>
          </h2>
          <div className = 'moneyContainer'>
            {coins.map(x=>
              <Coin key={x.value} value = {x.value} dimension = {x.dimension}  color = {x.color} coinClickHandle = {coinClickHandle}/>
            )}
          </div>
    </div>


    );
  }
  
  export default Inventory;
  