import { CartCheck } from "react-bootstrap-icons";
function VendingButton(props) {
  

  let {label,buttonPress,_id,canPurchase,canBeDisabled} = props
  return (
        <button onClick = {() => buttonPress(_id)}  className = 'keyPadContainerChild' disabled = {!canPurchase&&canBeDisabled}>
          {_id === 'buy'? <CartCheck/> : null}
          {label}
        </button>
  );
}

export default VendingButton;
