import './App.css';
import VendingKeyPad from "./components/vendingKeyPad";
import Screen from './components/screen';
import Inventory from './components/inventory'
import Table from './components/table'
import {useState,useEffect} from 'react';
import WebFont from 'webfontloader'
import {maxLimitOnScreen,initialProducts} from './config'
import Products from "./components/products";
import {getRecordById,getRecordNumberById} from "./utils/objectUtils"
import { instructions } from "./config";
import BottomStats from './components/bottomStats'

function App() {

  useEffect(() => {
    WebFont.load({
      google: {
        families: ['Tourney']
      }
    });
    document.title = 'Vending Machine'
   }, []);


  const [display,setDisplay] = useState('II')
  const [canPurchase,setCanPurchase] = useState(false)
  const [funds,setFunds] = useState(0)
  const [products,setProducts] = useState(initialProducts)
  const [bought,setBought] = useState([])

  const coinClickHandle = function(value){
    setFunds(funds+value)
  }

  const getProductOnScreen = function(whatToDisplay){
    const screenID = parseInt(whatToDisplay);
    for (const iterator of products) {
      if (iterator.id === screenID) {
        return iterator.id
      }
    }
    return false
  }

  const clear = function () {
    setDisplay('II')
  }

  const buttonPress = function (id) {
    let whatToDisplay = display

    
    if (whatToDisplay.length >= maxLimitOnScreen) {
      whatToDisplay = id
    }else{
      whatToDisplay = whatToDisplay+id
    }

    const productOnScreen = getProductOnScreen(whatToDisplay)
    // console.log(productOnScreen);

    setDisplay(whatToDisplay)
    setCanPurchase(!!productOnScreen)
    
  }

  const buy = function(){
    const id = parseInt(display) ;
    if(!id){
      return
    }
    const product = getRecordNumberById(products,id)
    const newProducts = products.map(x=>{
      if (x.id === id) {
        const newQty = x.qty-1
        return {...x, qty: newQty}
      }else{
        return x
      }
    }) 
    const newFunds = funds - newProducts[product].price

    
    const productAlreadyGot = getRecordById(bought,id)
    const boughtQty = productAlreadyGot?productAlreadyGot.qty+1:1
    const productObj = {...getRecordById(products,id),qty:boughtQty}
    const boughtStuff = bought.filter(x=>x.id !== id)
    boughtStuff.push(productObj)

    if (newFunds<0) {
      setDisplay('XX')
      setCanPurchase(false)
      return
    }
    if (newProducts[product].qty<0) {
      setDisplay('QQ')
      setCanPurchase(false)
      return
    }
    setFunds(newFunds)
    setProducts(newProducts)
    setCanPurchase(false)
    setDisplay('II')
    setBought(boughtStuff)
  }

  return (
    <div className="App">
      <header className="App-header">

      </header>
      <div className = 'machineGridContainer'>
        <div className = 'machineContainer'>
          <div className = 'item-a'>
            <Products products = {products}/>
          </div>
          <div className = 'item-b'>
            <Screen display = {display}/>
            <VendingKeyPad clear={clear} buttonPress = {buttonPress} canPurchase = {canPurchase} buy = {buy}/>
          </div>
          <div className = 'item-c'>
            
          </div>
          <div className = 'item-d'>
            <Inventory funds = {funds} coinClickHandle = {coinClickHandle}/>
          </div>
          <div className = 'item-e'>
            <Table data = {instructions} title='Screen Messages'/>  
          </div>
        </div>
      </div>
      <BottomStats bought={bought} title='Stats'/>
    </div>
  );
}

export default App;
